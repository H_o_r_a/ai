import time
import numpy
import os
import pandas as pd

from encoder import Encoder
from sp import SP
from union import Union
from scipy.special import softmax


class Model:
	def __init__(self, encoders=list(), events=list(), show_time=False,dimension = 2048):
		self.start_time = time.time()
		self.encoders_list = encoders
		self.events_list = events
		self.encoder = dict()
		self.union = dict()
		self.encoded = []
		self.buff = []
		self.puller = []
		self.dimension = dimension
		self.union1 = list()

		self.sp = None
		self.show_time = show_time
		
		# Shot time point to console
		self.time_point('Script start')
		
		# Create Encoders
		self.create_encoder()
		
		self.enc_width = self.get_enc_width()
		
		# Create Unions for events
		self.create_union()

		# Create Spatial Pooler
		self.create_sp()
		
	def time_point(self, point):
		if not self.show_time:
			return
		
		time_diff = round(time.time() - self.start_time, 2)
		print (point, '=>', time_diff)
	
	def create_encoder(self):
		for x in self.encoders_list:
			self.encoder[x['name']] = Encoder(name=x['name'], resolution=x['resolution'])
		
		self.time_point('Creating encoders done')
	
	def get_enc_width(self):
		enc_width = 0
		for key, val in self.encoder.items():
			enc_width += val.get_width()
		
		return enc_width
	
	def create_union(self):
		for x in self.events_list:
			self.union[x] = Union(self.enc_width, str(x) + '-union')
		
		self.time_point('Creating Unions done')
	
	#Add numpy.nonzero in values before expression
	def get_union(self, name=None):
		if name is None:
			for x in self.union:
				print {'title': x, 'values': (self.union[x].getUnion())}
				print {'title': x, 'len union': len(self.union[x].getUnion())}
				print {'title': x, 'len active union': len(numpy.nonzero(self.union[x].getUnion()))}

		else:
			print {'title': name, 'values': (self.union[name].getUnion())}
		
	def create_sp(self):
		self.sp = SP(width=self.enc_width, dimension = self.dimension)
		
		self.time_point('Creating Spatial Pooler done')
	
	def learning(self, df=None, idx=1):
		if df is None:
			return
		
		# curr_dir = os.path.dirname(os.path.abspath(__file__))
		# df.to_csv(curr_dir + '/data/' + str(idx) + '.csv')
		# for x in df.iterrows():
		# 	print x
		# 	print '*'*100
		#
		# print "\n"*10
		# exit()

		self.time_point('Start learning')
		counter = 0
		for index, row in df.iterrows():
			enc = []
			for key, val in self.encoder.items():
				encoded = self.encoder[key].encode_to_array(row[key])
				enc.append(encoded)
            
			encoded = numpy.concatenate(enc)
    
			result = self.sp.compute(encoded)
            
			for event in self.events_list:
				# print row
				if int(row[event]) == 1:
					self.union[event].add(result)
			# counter += 1
			# if counter >= 500:
			# 	break
		self.time_point('Learning done')

		self.sp.save()

		self.time_point('Saving Spatial Pooler done')
		self.encoded = encoded 							
		# Encoder which we visualize
		self.buff = enc 								
		# Array of encoders in the loop, to numpy.concatenate for decoder Encoder
		self.puller = result
		
        
		# for index, row in self.union.iteritems():
		# 	gu = row.getUnion()
		# 	print index
		# 	print type(gu)
		# 	print (gu)
		# 	print numpy.nonzero(gu)
		# 	print "\n\n"

		for key, val in self.union.items():
			self.union[key].save()

		#self.union1 = self.get_union()
		self.time_point('Saving unions done')

	enc = list()
	def classify(self, df=None, verbosity=False):
		res_list = list()
		soft_max_array = []
		for index, row in df.iterrows():
			# print index
			# print row
			#
			# continue
			# exit()
			enc = list()
			for key, val in self.encoder.items():
				if float(row[key]) > 1:
					print (index)
					print (('key'),key)
					print (row[key])
					# print row
					print ('*'*30)
				encoded = self.encoder[key].encode_to_array(row[key])
				enc.append(encoded)
			
			encoded = numpy.concatenate(enc)
			result = self.sp.compute(encoded, False)

			# print numpy.nonzero(result)
			# print len(numpy.nonzero(result)[0])
			
			res_str = ''
			res = dict()
			
			res['real'] = None
			res['pressure'] = row['pressure']
			res['flow'] = row['flow']
			res['duration'] = row['duration']
			res['real_pressure'] = row['real_pressure']
			res['real_flow'] = row['real_flow']
			res['real_duration'] = row['real_duration']
			
			for event in self.events_list:
				classify_res = self.union[event].check(result)
				
				if event in row and int(row[event]) == 1:
					res_str += 'real event => ' + event
					res_str += "\n"
					res['real'] = event

					print(('Classify res'), classify_res['percent'])
					print(('Classify res type'), type(classify_res))

				soft_max_array.append(classify_res['percent']/100)
				# print('T'), type(res['smax_tap'])
				res[event] = classify_res

				# print('Reset'), res[event]
				res_str += event + ' => ' + str(classify_res['percent']) + '; num => ' + str(classify_res['num'])
				res_str += "\n"
				print(('Type'), (soft_max_array))

				res_soft_max = softmax(soft_max_array)

			res['smax_tap'] = res_soft_max[0]
			res['smax_toilet'] = res_soft_max[1]
			res['smax_shower'] = res_soft_max[2]

			if verbosity:
				print(('persent1'), '-' * 100)
				print(('res'), res_str)
				# print('classify res'), classify_res['persent']
				# print row
				if event is None:
					print(('Row0'),row)
				if res['real'] is None:
					print(('Row1'), row)
				# print res_str
				# print numpy.nonzero(result)
			soft_max_array = []
			res_list.append(res)
			# print('Res list'), res_list
			print(('Res'), res)
		self.time_point('Classification done')
		return res_list
	
	def model_to_dFrame(self):
		dataFrame = pd.DataFrame(self.encoded)
		dataFrame.to_csv('my_csv.csv', index = True)
		return dataFrame
	
	


