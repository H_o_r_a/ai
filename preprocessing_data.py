import csv
import datetime
import os
import yaml
import math
import numpy as np
from itertools import islice
from time import time
import pandas as pd


class Data_Preparation:
    def __init__(self, df, max_variable_pressure=4.4, max_var_duration=400, max_variable_flow=4.2):
        # Output dataframe
        self.data = df
        self.max_flow_for_event = 4
        # Find edges in dataframe
        self.edges = None
        # Calculate diffs for edges
        self.diffs = None
        # Fist found edge
        self.first_edge = None
        # Second found edge
        self.second_edge = None
        self.start_baseline = 0
        # Slices for find baseline(median)
        self.slices_df = []
        # Array for baseline
        self.pressure_baseline = []
        self.flow_baseline_arr = []
        self.duration_arr = []
        # Variables maximum for normalization data
        self.max_variable_pressure = max_variable_pressure
        self.max_variable_flow = max_variable_flow
        self.max_var_duration = max_var_duration
        self.flow_baseline_val = None
        self.pressure_baseline_val = None
        self.low_tap = None
        self.high_tap = None
        self.high_tap_1 = None
        self.toilet = None
        self.toilet_1 = None
        self.shower = None
        self.shower_1 = None
        self.duration = None
        self.start = None
        self.file = None
        self.next_index = None
        self.result = []
        self.data_new = None
        self.prev_index = 0
        self.last_zero = 0
        self.last_value = None
        self.tap_low_arr = []
        self.tap_high_arr = []
        self.shower_arr = []
        self.toilet_arr = []
        self.prepared_data = pd.DataFrame({})
        self.pres = []
        self.durr = []
        self.fl = [0]
        self.start_id = None
        self.start_id_1 = None
        self.end_id = None
        self.end_id_1 = None
        self.start_time_id = None
        self.start_time_id_1 = None
        self.end_time_id = None
        self.end_time_id_1 = None
        self.start_id_arr = []
        self.end_id_arr = []
        self.start_time_id_arr = []
        self.end_time_id_arr = []
        self.no_start_id = None
        self.no_end_id = None
        self.no_start_time = None
        self.no_end_time = None
        self.data = df
        self.data = self.data
        self.val_massive = []
        self.val_massive_flow = []
        self.id_baseline = []
        self.pressure_baseline = []
        self.flow_baseline = []
        self.baseline_test = []
        self.counter_none_flow = 0
        self.counter = 0
        self.baseline_flow = []
        self.sum_sred_1 = None
        self.aver_flow = None
        self.test_flow = []
        self.test_pres = []
        self.test_dur = []
        self.max_value = 0
        self.max_ind = 0
        self.overlap_events_pressure = 0
        self.real_label = []
        self.test_label = []

        self.overlap_events_flow = 0
        #### Delete elements
        self.first_slice = None
        self.second_slice = None
        self.first_slice_pres = None
        self.second_slice_pres = None
        self.first_slice_flow = None
        self.second_slice_flow = None
        self.graph_array = []

        self.start_idx = []
        self.end_idx = []

        self.base = None

    def separate_by_sensors(self):
        self.data.fillna(0, inplace=True)
        self.data.replace({'--', 0})
        # self.data = self.data.astype(float)
        if 'toilet flow' in self.data:
            for d in range(len(self.data)):

                if self.data['faucet flow'].iloc[d] > 0.23:
                    self.real = 1
                    # print self.real
                elif self.data['toilet flow'].iloc[d] > 0.23:
                    self.real = 2     #'toilet'
                    # print self.real
                elif self.data['shower flow'].iloc[d] > 0.23:
                    self.real = 3           #'high_tap'
                    # print self.real
                else:
                    self.real = 0

                self.real_label.append(self.real)
            self.v = np.zeros(len(self.data))
            self.data['event'] = self.real_label
            self.data['label'] = self.v

        if type(self.data['event'][0]) == str:
            self.data.fillna(0, inplace=True)
            # print data['event']
            print('Overlap')
            self.data['event'] = self.data['event'].astype(str)
            new = self.data["event"].str.split(",", n=1, expand=True)
            self.data["temp_shower"] = new[0]
            self.data["temp_tap"] = new[1]
            self.data['temp_shower'] = pd.to_numeric(self.data['temp_shower'], errors='coerce')
            self.data['temp_tap'] = pd.to_numeric(self.data['temp_tap'], errors='coerce')
            self.data['shower'] = self.data['temp_shower'].where(self.data['temp_shower'] == 3, 0).replace(3, 1)
            self.data['toilet'] = self.data['temp_shower'].where(self.data['temp_shower'] == 2, 0).replace(2, 1)
            # self.data['low_tap'] = self.data['temp_shower'].where(self.data['temp_shower'] == 1, 0).replace(2, 1)
            self.data['high_tap'] = self.data['temp_shower'].where(self.data['temp_shower'] == 1, 0).replace(2, 1)
            self.data['shower_a'] = self.data['temp_tap'].where(self.data['temp_tap'] == 3, 0).replace(3, 1)
            self.data['toilet_a'] = self.data['temp_tap'].where(self.data['temp_tap'] == 2, 0).replace(2, 1)
            # self.data['low_tap_a'] = self.data['temp_tap'].where(self.data['temp_tap'] == 1, 0).replace(2, 1)
            self.data['high_tap_a'] = self.data['temp_tap'].where(self.data['temp_tap'] == 1, 0).replace(2, 1)
            self.data['shower'] += self.data['shower_a']
            self.data['toilet'] += self.data['toilet_a']
            # self.data['low_tap'] += self.data['low_tap_a']
            self.data['high_tap'] += self.data['high_tap_a']
            self.data = self.data.drop(['temp_shower', 'temp_tap', 'high_tap_a', 'toilet_a', 'shower_a'], axis=1)
            # print self.data[3940:3950]
            print('Separate sensors done')
        else:
            print('Reconfigure')
            self.data.fillna(0, inplace=True)
            self.data['temp_shower'] = pd.to_numeric(self.data['event'], errors='coerce')
            self.data['shower'] = self.data['temp_shower'].where(self.data['temp_shower'] == 3, 0).replace(3, 1)
            self.data['toilet'] = self.data['temp_shower'].where(self.data['temp_shower'] == 2, 0).replace(2, 1)
            self.data['high_tap'] = self.data['temp_shower'].where(self.data['temp_shower'] == 1, 0).replace(2, 1)

    def cut_zero(self):
        self.data_new = self.data
        self.data_new.to_csv('data_new.csv')

    def baseline_data(self, left_r):
        self.val_massive = []
        self.val_massive_flow = []
        self.val_massive.append(self.pressure_baseline[left_r])
        self.val_massive_flow.append(self.flow_baseline[left_r])
        if len(self.val_massive) > 1:
            for i in range(2, len(self.pressure_baseline)):
                if abs(self.flow_baseline[left_r + i - 1] - self.flow_baseline[left_r + i - 2]) < 1 or len(self.val_massive_flow)<5:
                    pass
                else:
                    del self.val_massive[-1]
                    del self.val_massive_flow[-1]
                try:
                    self.sum_sred_1 = sum(self.val_massive) / len(self.val_massive)
                    self.aver_flow = sum(self.val_massive_flow) / len(self.val_massive_flow)
                except ZeroDivisionError:
                    print('Zero-0')
            self.test_dur.append(len(self.val_massive))
            self.test_pres.append(round(self.sum_sred_1, 2))
            self.test_dur.append(round(self.aver_flow, 2))
        else:
            self.val_massive.append(self.pressure_baseline[left_r + 1])
            self.val_massive_flow.append(self.flow_baseline[left_r + 1])
            # print('Val massive-0'), self.val_massive_flow

            for i in xrange(2, (len(self.pressure_baseline))):
                try:
                    if abs(self.flow_baseline[i - 1] - self.flow_baseline[i - 2]) < 1 or len(self.val_massive_flow)<5:
                        try:
                            self.val_massive.append(self.pressure_baseline[left_r + i])
                            self.val_massive_flow.append(self.flow_baseline[left_r + i])
                            if len(self.pressure_baseline) - 1 == left_r + i:
                                try:
                                    self.sum_sred_1 = sum(self.val_massive) / len(
                                        self.val_massive)  # np.mean(self.val_massive)
                                    self.aver_flow = sum(self.val_massive_flow) / len(self.val_massive_flow)  # np.mean(self.val_massive_flow)
                                except ZeroDivisionError:
                                    print('Zero-1')
                                self.test_dur.append(len(self.val_massive))
                                self.test_pres.append(round(self.sum_sred_1, 2))
                                self.test_flow.append(round(self.aver_flow, 2))
                        except IndexError:
                            print('Index-0')
                    else:
                        # print('Answer'),  self.flow_baseline[i - 1], '-' ,self.flow_baseline[i - 2], '->',self.flow_baseline[i - 1] - self.flow_baseline[i - 2]

                        # if len(self.val_massive) < 3:
                        #     self.val_massive.append(self.pressure_baseline[left_r + i])
                        #     self.val_massive_flow.append(self.flow_baseline[left_r + i])
                        #
                        # else:
                        if len(self.val_massive) == 0:
                            self.val_massive.append(self.pressure_baseline[left_r + i])
                            self.val_massive_flow.append(self.flow_baseline[left_r + i])
                        else:
                            del self.val_massive[-1]
                            del self.val_massive_flow[-1]
                        try:
                            self.sum_sred_1 = sum(self.val_massive) / len(self.val_massive)
                            self.aver_flow = sum(self.val_massive_flow) / len(self.val_massive_flow)
                        except ZeroDivisionError:
                            pass
                            # print('Zero-2')
                        # pass
                        # print('Answer'), self.flow_baseline[i - 1] - self.flow_baseline[i - 2]
                        # print('Average flow'), self.aver_flow
                        # print('Val massive'), len(self.val_massive)
                        self.test_dur.append(len(self.val_massive))
                        self.test_flow.append(round(self.aver_flow, 2))
                        self.test_pres.append(round(self.sum_sred_1, 2))

                        # print('Test flow0'), self.test_flow
                        # print('Test pressure0'), self.test_pres
                        # print('Test durration0'), self.test_dur
                        self.val_massive = []
                        self.val_massive_flow = []
                # break
                except IndexError:
                    print('IndexError last')
        self.index = 0

    def calling_baseline_real_data(self):
        counter = 0
######READY

        # for t in range(len(self.data_new)):
        #     if self.data_new['flow'][t] < 0.23:
        #         self.base = self.data_new['pressure'][t]
        #         break
        # print('B'), self.base
        edges_df = np.where((self.data_new["flow"] > 0.23) & (self.data_new["flow"].shift(1) < 0.23) | \
                            (self.data_new["flow"] < 0.23) & (self.data_new["flow"].shift(1) > 0.23))[0]
        # print len(edges_df)
        if len(edges_df) == 1:
            print len('Edges df = 1')
            slice2val = (edges_df[0], len(self.data_new) - 1)
            self.slices_df.append(slice2val)
        else:
            for e_df, edge_df in np.ndenumerate(edges_df[self.start:len(edges_df) - 1]):
                idx = e_df[0]
                # print edges_df[idx+1], '-', edges_df[idx], '=>', edges_df[idx+1] - edges_df[idx]
                if edges_df[idx + 1] - edges_df[idx] > 3:
                    slice2val = (edges_df[idx], edges_df[idx + 1])
                    self.slices_df.append(slice2val)
        if self.data_new['flow'][0] != 0:
            if len(edges_df) == 0:
                start_slice = (0, self.data_new['flow'][0])
            else:
                start_slice = (0, edges_df[0])
            self.slices_df.insert(0, start_slice)
        # print('Slices df'), self.slices_df[:20]
        if len(edges_df) == 0:
            self.flow_baseline_val = self.data_new['flow'][0:(len(self.data_new) - 1)].median()
            self.pressure_baseline_val = self.data_new['pressure'].iloc[0:(len(self.data_new) - 1)].median()
            self.duration = self.data_new['pressure'].iloc[0:(len(self.data_new) - 1)].count()

            if 'id' in self.data:
                self.start_id = self.data_new['id'][0]
                self.end_id = self.data_new['id'][len(self.data_new) - 1]
                self.start_time_id = self.data_new['time'][0]
                self.end_time_id = self.data_new['time'][len(self.data_new) - 1]
                if len(self.data_new['id'].dropna()) == 0:
                    print("Column 'id' is empty")
                    self.start_id = 0
                    self.end_id = 0
                if len(self.data_new['time'].dropna()) == 0:
                    print("Column 'time' is empty")
                    self.start_time_id = 0
                    self.end_time_id = 0
                else:
                    self.start_id = self.data_new['id'][0]
                    self.end_id = self.data_new['id'][1]
                    self.start_time_id = self.data_new['time'][0]
                    self.end_time_id = self.data_new['time'][1]

            self.start_id_arr.append(self.start_id)
            self.end_id_arr.append(self.end_id)
            self.start_time_id_arr.append(self.start_time_id)
            self.end_time_id_arr.append(self.end_time_id)

            self.pressure_baseline.append(self.pressure_baseline_val)
            self.flow_baseline_arr.append(self.flow_baseline_val)
            self.duration_arr.append(self.duration)

            self.start_idx.append(self.data_new['real_flow'].index[0])
            self.end_idx.append(self.data_new['real_flow'].index[1])

            self.prepared_data['flow'] = self.flow_baseline_arr
            self.prepared_data['pressure'] = self.pressure_baseline
            self.prepared_data['duration'] = self.duration_arr
            self.prepared_data['start index'] = self.start_idx
            self.prepared_data['end index'] = self.end_idx

        else:
            for slice_idx_df in self.slices_df:
                # print slice_idx_df[0], ':', slice_idx_df[1]

                if slice_idx_df[1] - slice_idx_df[0] == 1:
                    self.flow_baseline_val = self.data_new['flow'].iloc[slice_idx_df[0]]
                    self.pressure_baseline_val = self.data_new['pressure'].iloc[slice_idx_df[0]]
                    self.duration = 1

                    if 'id' in self.data:
                        self.start_id = self.data_new['id'][0]
                        self.end_id = self.data_new['id'][len(self.data_new) - 1]
                        self.start_time_id = self.data_new['time'][0]
                        self.end_time_id = self.data_new['time'][len(self.data_new) - 1]

                        if len(self.data_new['id'].dropna()) == 0:
                            print("Column 'id' is empty")
                            self.start_id = 0
                            self.end_id = 0
                        if len(self.data_new['time'].dropna()) == 0:
                            print("Column 'time' is empty")
                            self.start_time_id = 0
                            self.end_time_id = 0
                        else:
                            self.start_id = self.data_new['id'][0]
                            self.end_id = self.data_new['id'][1]
                            self.start_time_id = self.data_new['time'][0]
                            self.end_time_id = self.data_new['time'][1]

                elif slice_idx_df[1] - slice_idx_df[0] > 2:
                    start = 1
                    self.pressure_baseline = self.data_new['pressure'][slice_idx_df[0]:slice_idx_df[1]].tolist()
                    self.flow_baseline = self.data_new['flow'][slice_idx_df[0]:slice_idx_df[1]].tolist()

                    # Baseline for overlap events
                    self.baseline_data(start - 1)

                    # if self.data_new['flow'][slice_idx_df[0]] > 0.23:
                    # print('Test flow-0'), (self.test_flow)
                    # print('Test duration0'), (self.test_dur)
                    # print('Test pressure0'), (self.test_pres)
                    #
                    # print('Length baseline flow'), len(self.baseline_flow)
                    try:
                        self.base = self.baseline_test[-1]
                    except IndexError:
                        self.base = self.data_new['pressure'][0]

                    self.start_index = slice_idx_df[0]
                    self.end_index = slice_idx_df[1]
                    # print('Start index'), self.start_index
                    # print('End index'), self.end_index

                    dur = 0
                    v = slice_idx_df[0]
                    t = 0
                    # start = slice_idx_df[0] + dur
                    for t in range(len(self.test_flow)-1):
                        if self.test_dur[t] < 3:
                            self.test_dur[t] = (self.test_dur[t] + self.test_dur[t-1])/2
                            self.test_flow[t] = (self.test_flow[t] + self.test_flow[t-1])/2
                            self.test_pres[t] = (self.test_pres[t] + self.test_pres[t-1])/2


                        # dur += self.test_dur[t]
                        #
                        # self.slice4grahp = (v, v+self.test_dur[t])
                        # self.graph_array.append(self.slice4grahp)
                        # v = v + self.test_dur[t]

                    # print('Graph array'), self.graph_array
                    if 'label' in self.data:
                        if len(self.test_flow) >= 2:
                            for v in range(len(self.test_dur)):
                                dur += self.test_dur[v]
                                self.test_label.append(self.data['label'][slice_idx_df[0]+dur-1])
                        else:
                            self.test_label.append(self.data['label'][slice_idx_df[0]])

                    if len(self.test_flow) > 2:
                        max_val = np.max(self.test_flow)
                        self.max_ind = self.test_flow.index(max_val)
                        # print('Max ind'), self.max_ind
                        self.first_slice = self.test_dur[self.max_ind - 1]
                        self.second_slice = self.test_dur[self.max_ind]

                        if self.max_ind == len(self.test_pres)-1:
                            self.third_slice = self.test_dur[self.max_ind]
                            self.third_slice_flow = self.test_flow[self.max_ind]
                            self.third_slice_pres = self.test_pres[self.max_ind]

                        else:
                            self.third_slice = self.test_dur[self.max_ind+1]
                            self.third_slice_flow = self.test_flow[self.max_ind + 1]
                            self.third_slice_pres = self.test_pres[self.max_ind + 1]

                        # print('First slice'), self.first_slice
                        # print('Second slice'), self.second_slice
                        # print('Third slice'), self.third_slice

                        self.first_slice_flow = self.test_flow[self.max_ind - 1]
                        self.second_slice_flow = self.test_flow[self.max_ind] - self.test_flow[self.max_ind-1]

                        # print('First slice flow'), self.first_slice_flow
                        # print('Second slice flow'), self.second_slice_flow
                        # print('Third slice flow'), self.third_slice_flow

                        self.first_slice_pres = self.test_pres[self.max_ind - 1]
                        self.second_slice_pres = self.test_pres[self.max_ind]

                        # print('First slice pressure'), self.first_slice_pres
                        # print('Second slice pressure'), self.second_slice_pres
                        # print('Third slice pressure'), self.third_slice_pres

                        min = self.test_flow[self.max_ind-1] - 0.5
                        max = self.test_flow[self.max_ind-1] + 0.5

                        try:
                             last_value = self.test_flow[self.max_ind + 1]
                        except IndexError:
                            last_value = self.test_flow[self.max_ind]

                        if min < last_value < max:
                            self.first_slice_temp = self.first_slice + self.second_slice + self.third_slice
                            self.second_slice_temp = self.second_slice

                            # print('First slice'), self.first_slice_temp
                            # print('Second slice'), self.second_slice_temp

                            self.first_slice_flow_temp = self.first_slice_flow
                            self.second_slice_flow_temp = self.second_slice_flow

                            # print('First slice flow temp'), self.first_slice_flow_temp
                            # print('Second slice flow temp'), self.second_slice_flow_temp

                            self.first_slice_pres_temp = self.first_slice_pres
                            # print('Base:'), self.base, '-', self.first_slice_pres-self.second_slice_pres
                            self.second_slice_pres_temp = self.base - (self.first_slice_pres-self.second_slice_pres)

                            # print('First slice presure temp'), self.first_slice_pres_temp
                            # print('Second slice pressure temp'), self.second_slice_pres_temp

                            if 'label' in self.data:
                                self.high_tap = self.data_new['high_tap'][slice_idx_df[0]+2]
                                # print('High Tap'), self.high_tap, slice_idx_df[0]+2
                                self.high_tap_1 = self.data_new['high_tap'][slice_idx_df[0] + self.first_slice + 2]
                                # print('High Tap-1'), self.high_tap_1, slice_idx_df[0] + self.first_slice + 2

                                self.toilet = self.data_new['toilet'][slice_idx_df[0]+2]
                                # print('Toilet'), self.toilet, slice_idx_df[0]+2
                                # if (self.toilet and self.high_tap_1):
                                #     self.toilet_1 = 0
                                # else:
                                self.toilet_1 = self.data_new['toilet'][slice_idx_df[0] + self.first_slice + 2]
                                # print('Toilet-1'), self.toilet_1, slice_idx_df[0] + self.first_slice + self.second_slice + 2

                                self.shower = self.data_new['shower'][slice_idx_df[0]+2]
                                # print('Shower'), self.shower, slice_idx_df[0]+2
                                if self.high_tap_1 or self.toilet_1:# or self.toilet:
                                    self.shower_1 = 0
                                else:
                                    self.shower_1 = self.data_new['shower'][slice_idx_df[0] + self.first_slice + 2]
                                # print('Shower-1'), self.shower_1, slice_idx_df[0] + self.first_slice + 2

                            if 'id' in self.data:
                                self.start_id = self.data_new['id'][slice_idx_df[0]]
                                self.start_id_1 = self.data_new['id'][slice_idx_df[0]+self.first_slice_temp]

                                self.end_id = self.data_new['id'][slice_idx_df[1]]

                                self.end_id_1 = self.data_new['id'][slice_idx_df[1]-self.third_slice]

                                self.start_time_id = self.data_new['time'][slice_idx_df[0]]
                                self.start_time_id_1 = self.data_new['time'][slice_idx_df[0]+self.first_slice_temp+1]

                                self.end_time_id = self.data_new['time'][slice_idx_df[1]]
                                self.end_time_id_1 = self.data_new['time'][slice_idx_df[1]-self.third_slice]

                            self.start_index = slice_idx_df[0]
                            self.start_index_1 = slice_idx_df[0] + self.first_slice

                            self.end_index = slice_idx_df[1]
                            self.end_index_1 = slice_idx_df[0] + self.first_slice + self.second_slice

                        else:
                            self.first_slice_temp = self.first_slice + self.second_slice

                            if self.max_ind == len(self.test_flow) - 1:
                                self.second_slice_temp = self.second_slice + self.third_slice
                            else:
                                self.second_slice_temp = self.test_dur[self.max_ind] + self.test_dur[self.max_ind+1]

                            # print('First slice  temp-1'), self.first_slice_temp
                            # print('Second slice  temp-1'), self.second_slice_temp

                            self.first_slice_flow_temp = self.first_slice_flow
                            self.second_slice_flow_temp = self.third_slice_flow

                            self.first_slice_pres_temp = self.first_slice_pres
                            self.second_slice_pres_temp = self.third_slice_pres

                            # print('First slice flow'), self.first_slice_flow_temp
                            # print('Second slice flow'), self.second_slice_flow_temp
                            #
                            # print('First slice pressure'), self.first_slice_pres_temp
                            # print('Second slice pressure'), self.second_slice_pres_temp

                            if 'label' in self.data:
                                self.high_tap = self.data_new['high_tap'][slice_idx_df[0]+2]
                                # print('High Tap'), self.high_tap, slice_idx_df[0]+2
                                self.high_tap_1 = self.data_new['high_tap'][slice_idx_df[1]-2]
                                # print('High Tap-1'), self.high_tap_1, slice_idx_df[1]-2

                                self.toilet = self.data_new['toilet'][slice_idx_df[0]+2]
                                # print('Toilet'), self.toilet, slice_idx_df[0]+2
                                self.toilet_1 = self.data_new['toilet'][slice_idx_df[1]-2]
                                # print('Toilet-1'), self.toilet_1, slice_idx_df[1]-2

                                self.shower = self.data_new['shower'][slice_idx_df[0]+2]
                                # print('Shower'), self.shower, slice_idx_df[0]+2
                                self.shower_1 = self.data_new['shower'][slice_idx_df[1]-2]
                                # print('Shower-1'), self.shower_1, slice_idx_df[1]-2

                            if 'id' in self.data:
                                self.start_id = self.data_new['id'][slice_idx_df[0]]
                                self.start_id_1 = self.data_new['id'][slice_idx_df[0]+self.first_slice_temp+1]

                                self.end_id = self.data_new['id'][slice_idx_df[0]+self.first_slice_temp\
                                                                  +self.test_dur[self.max_ind]]
                                self.end_id_1 = self.data_new['id'][slice_idx_df[1]]

                                self.start_time_id = self.data_new['time'][slice_idx_df[0]]
                                self.start_time_id_1 = self.data_new['time'][slice_idx_df[0]+self.first_slice_temp+1]

                                self.end_time_id = self.data_new['time'][slice_idx_df[0] + self.first_slice_temp+self.test_dur[self.max_ind]]
                                self.end_time_id_1 = self.data_new['time'][slice_idx_df[1]]

                            self.start_index = slice_idx_df[0]
                            self.start_index_1 = slice_idx_df[0] + self.first_slice

                            self.end_index = slice_idx_df[0]+self.first_slice+self.second_slice
                            self.end_index_1 = slice_idx_df[1]

                        if 'label' in self.data:

                            self.tap_high_arr.append(self.high_tap)
                            self.toilet_arr.append(self.toilet)
                            self.shower_arr.append(self.shower)

                            self.tap_high_arr.append(self.high_tap_1)
                            self.toilet_arr.append(self.toilet_1)
                            self.shower_arr.append(self.shower_1)

                        if 'id' in self.data:
                            self.start_id_arr.append(self.start_id)
                            self.start_id_arr.append(self.start_id_1)
                            self.end_id_arr.append(self.end_id)
                            self.end_id_arr.append(self.end_id_1)
                            self.start_time_id_arr.append(self.start_time_id)
                            self.start_time_id_arr.append(self.start_time_id_1)
                            self.end_time_id_arr.append(self.end_time_id)
                            self.end_time_id_arr.append(self.end_time_id_1)

                        self.duration_arr.append(self.first_slice_temp)
                        self.baseline_test.append(self.first_slice_pres_temp)
                        self.baseline_flow.append(self.first_slice_flow_temp)

                        self.start_idx.append(self.start_index)
                        self.end_idx.append(self.end_index)

                        self.duration_arr.append(self.second_slice_temp)
                        self.baseline_test.append(self.second_slice_pres_temp)
                        self.baseline_flow.append(self.second_slice_flow_temp)

                        self.start_idx.append(self.start_index_1)
                        self.end_idx.append(self.end_index_1)

                    else:
                        if 'id' in self.data:
                            self.start_id = self.data_new['id'][slice_idx_df[0]]
                            self.end_id = self.data_new['id'][slice_idx_df[1]]

                            self.start_time_id = self.data_new['time'][slice_idx_df[0]]
                            self.end_time_id = self.data_new['time'][slice_idx_df[1]]

                        self.duration_arr.append(self.test_dur[0])
                        self.baseline_test.append(self.test_pres[0])
                        self.baseline_flow.append(self.test_flow[0])

                        self.start_idx.append(self.start_index)
                        self.end_idx.append(self.end_index)

                        if 'id' in self.data:
                            self.start_id_arr.append(self.start_id)
                            self.end_id_arr.append(self.end_id)
                            self.start_time_id_arr.append(self.start_time_id)
                            self.end_time_id_arr.append(self.end_time_id)

                        if 'label' in self.data:
                            self.high_tap = self.data_new['high_tap'][slice_idx_df[0]]
                            self.toilet = self.data_new['toilet'][slice_idx_df[0]]
                            self.shower = self.data_new['shower'][slice_idx_df[0]]

                            self.tap_high_arr.append(self.high_tap)
                            self.toilet_arr.append(self.toilet)
                            self.shower_arr.append(self.shower)

                elif slice_idx_df[1] - slice_idx_df[0] == 2:
                    self.baseline_flow.append((slice_idx_df[0] + slice_idx_df[1]) / 2)
                    self.duration_arr.append(slice_idx_df[0] + slice_idx_df[1])
                    self.baseline_test.append((slice_idx_df[0] + slice_idx_df[1]) / 2)
                    if 'label' in self.data:
                        self.high_tap = self.data_new['high_tap'][slice_idx_df[0]]
                        self.toilet = self.data_new['toilet'][slice_idx_df[0]]
                        self.shower = self.data_new['shower'][slice_idx_df[0]]
                    if 'id' in self.data:
                        self.start_id = self.data_new['id'][slice_idx_df[0]]
                        self.end_id = self.data_new['id'][slice_idx_df[1]]
                        self.start_time_id = self.data_new['time'][slice_idx_df[0]]
                        self.end_time_id = self.data_new['time'][slice_idx_df[1]]
                    self.tap_high_arr.append(self.high_tap)
                    self.toilet_arr.append(self.toilet)
                    self.shower_arr.append(self.shower)
                    self.start_id_arr.append(self.start_id)
                    self.end_id_arr.append(self.end_id)
                    self.start_time_id_arr.append(self.start_time_id)
                    self.end_time_id_arr.append(self.end_time_id)

                else:
                    if 'label' in self.data:
                        self.tap_high_arr.append(self.data_new['high_tap'][slice_idx_df[0]])
                        self.toilet_arr.append(self.data_new['toilet'][slice_idx_df[0]])
                        self.shower_arr.append(self.data_new['shower'][slice_idx_df[0]])
                    if 'id' in self.data:
                        self.start_id_arr.append(self.data_new['id'][slice_idx_df[0]])
                        self.end_id_arr.append(self.data_new['id'][slice_idx_df[1]])
                        self.start_time_id_arr.append(self.data_new['time'][slice_idx_df[0]])
                        self.end_time_id_arr.append(self.data_new['time'][slice_idx_df[1]])
                    self.baseline_flow.append(round(self.aver_flow, 2))
                    self.duration_arr.append(len(self.val_massive))
                    self.baseline_test.append(round(self.sum_sred_1, 2))
                # print('Test flow main'), self.test_flow

                # Empty array
                self.test_flow = []
                self.test_dur = []
                self.test_pres = []

            # print('Length pressure'), len(self.baseline_test)
            if len(self.baseline_flow) == 0:
                self.no_event()
            else:
                self.prepared_data['start index'] = self.start_idx
                self.prepared_data['end index'] = self.end_idx

                self.prepared_data['pressure'] = self.baseline_test
                self.prepared_data['flow'] = self.baseline_flow
                self.prepared_data['duration'] = self.duration_arr
                if 'label' in self.data:
                    # print('Length tap'), len(self.tap_high_arr)
                    self.prepared_data['high_tap'] = self.tap_high_arr
                    self.prepared_data['toilet'] = self.toilet_arr
                    self.prepared_data['shower'] = self.shower_arr
                if 'id' in self.data:
                    print('Length start_id'), len(self.start_id_arr)
                    self.prepared_data['start_id'] = self.start_id_arr
                    self.prepared_data['end_id'] = self.end_id_arr
                    self.prepared_data['start_time'] = self.start_time_id_arr
                    self.prepared_data['end_time'] = self.end_time_id_arr
                if len(self.prepared_data) == 1:
                    self.prepared_data['pressure'] = 0
                else:
                    # print('TUT')
                    # print 'Prepared 0', self.prepared_data['pressure']
                    self.prepared_data['real_pressure'] = self.prepared_data['pressure']
                    self.prepared_data['real_flow'] = self.prepared_data['flow']
                    self.prepared_data['real_duration'] = self.prepared_data['duration']
                    self.prepared_data['pressure'] = self.prepared_data['pressure'].diff(periods=-1)
                    self.prepared_data['difference_pressure'] = self.prepared_data['pressure']
                    self.last_value = self.prepared_data['pressure'].iloc[-2]
                    if self.prepared_data['flow'].iloc[-1]:
                        self.prepared_data.pressure.fillna(self.last_value, inplace=True)
                    else:
                        self.prepared_data.drop(self.prepared_data.tail(1).index, inplace=True)
                self.prepared_data.fillna(0, inplace=True)
                # print('Prepare-1'), (self.prepared_data)

    def no_event(self):
        print('No event')
        self.prepared_data = pd.DataFrame(columns=['flow', 'pressure', 'duration', 'start_id', 'end_id',
                                                   'start_time', 'end_time'])

        return self.prepared_data

    # Normalization data
    def normalizate_data(self):
        # print('Prepare-2'), len(self.prepared_data)
        if len(self.prepared_data) > 1:
            self.prepared_data = self.prepared_data[self.prepared_data['flow'] > 0.23]
            self.prepared_data = self.prepared_data[self.prepared_data['duration'] > 3]
            self.prepared_data = self.prepared_data.reset_index(drop=True)
        if len(self.prepared_data) == 0:
            self.no_event()
        else:
            self.prepared_data['pressure'] = abs(self.prepared_data['pressure'] / self.max_variable_pressure)
            self.prepared_data['flow'] = self.prepared_data['flow'] / self.max_variable_flow
            self.prepared_data['duration'] = self.prepared_data['duration'] / self.max_var_duration
        # print('Prepare-3'), (self.prepared_data)
        return self.prepared_data

    def save(self, file=None):
        self.file = file
        curr_dir = os.path.dirname(os.path.abspath(__file__))
        if self.file is None:
            self.file = 'file'
        else:
            self.file = file
        data_file_path = curr_dir + '/' + self.file + '.csv'
        self.prepared_data.to_csv(data_file_path)

    def calc_last_value(self):
        if not self.data['flow'].tail(1).item():
            self.last_zero = 0
        else:
            self.last_zero = 1

    def get_label(self):
        return self.last_zero

    # Main function, if dataframe have column label, we used function separate_by_sensors.
    def prepare(self):
        print('Version 1_18_14:15')
        if (self.data['flow'] > 0.23).any():
            if 'label' in self.data or 'toilet flow' in self.data:
                start_time_separate_sensors = time()
                self.separate_by_sensors()
                end_time_separate_sensors = time()
                print('Time separate sensors'), (end_time_separate_sensors - start_time_separate_sensors)
            # self.separate_by_sensors()
            # self.real2synthetic()
            start_time_cut_zero = time()
            self.cut_zero()
            end_time_cut_zero = time()
            print('Time cut zero'), (end_time_cut_zero - start_time_cut_zero)
            self.calling_baseline_real_data()
            self.normalizate_data()
            self.calc_last_value()
            print('Length dataframe after learn'), len(self.prepared_data)
            self.prepared_data.to_csv('after.csv')
            return self.prepared_data
        else:
            self.cut_zero()
            self.no_event()
            self.normalizate_data()
            print('Length dataframe after learn-ELSE'), len(self.prepared_data)
            self.prepared_data.to_csv('after.csv')
            return self.prepared_data


# if __name__ == "__main__":
#     print("LEARN")
#     Learn_DataSet = "toilet.csv"#"24h-Aqueous9876094869.csv" #"aidrome.csv"
#     data = pd.read_csv(Learn_DataSet)
#     prepare_data = Data_Preparation(data)
#     df = prepare_data.prepare()
#     print("Length datafame"), len(df)
#     # print('Dataframe'), df
