import os
import math
import time
import pandas
import numpy
import __future__
import matplotlib 
import numpy as np
from model01 import Model
import ipywidgets as widgets
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from IPython.display import display
from ipywidgets import interact,interactive
from preprocessing_data import Data_Preparation as DP

class Settings:
    # In this class the whole logic of the work will last.
    def __init__(self):
        self.curr_dir = os.path.dirname(os.path.abspath(__file__))
        print (self.curr_dir)
        self.sp_file_path = self.curr_dir + '/data/sp.tmp'
        self.df1 = None
        self.df2 = None
        self.df3 = None
        self.enc_array = []
        self.sp_array = []
        self.first_item = 0
        self.second_item = 0
        self.models = None
        self.readData()

   
    def readData(self):
        #Method in which preprocessing data is formed
        data_file_path1 = self.curr_dir + '/222/shower.csv'
        data_file_path2 = self.curr_dir + '/222/tap.csv'
        data_file_path3 = self.curr_dir + '/222/toilet.csv'

        data1 = pandas.read_csv(data_file_path1)
        prepare_data1 = DP(data1)

        data2 = pandas.read_csv(data_file_path2)
        prepare_data2 = DP(data2)

        data3 = pandas.read_csv(data_file_path3)
        prepare_data3 = DP(data3)

        self.df1 = prepare_data1.prepare()
        print ('*'*100)
        self.df2 = prepare_data2.prepare()
        print ('*'*100)
        self.df3 = prepare_data3.prepare()
        print ('*'*100)
    

    def redrawing(self,array, string):
        #A method that implements the visualization of the encoder and sp
        fig = plt.figure(figsize = (10, 10))
        ax = sns.heatmap(array, linewidths = 1 ,linecolor = "w",cbar = False,cmap = "nipy_spectral")
        ax.set_title(string)
        plt.gca().invert_yaxis()
        plt.axis("off")

    def calculate_array(self,m_array):
        # Method for recalculating an array for redraws
        rowCount = int(math.sqrt(len(m_array))) + 2
        colCount = int(math.sqrt(len(m_array)))
        massiv = [[0]*colCount for i in range(rowCount)]

        for iterator in range(len(m_array)):  
            row = int(iterator / colCount)
            col = int(iterator % colCount)
            massiv[row][col] = m_array[iterator]
            
        return massiv

    def createModel(self,enc = 1,sp = 2048,df = None):   
        print('ENC:'), enc
        print('SP_COL'),sp
        print('DF'),df
        #Creating a model
        md = Model(
            encoders=[
                {'name': "flow", 'resolution': enc},
                {'name': "pressure", 'resolution': enc},
                {'name': "duration", 'resolution': enc}
            ],
            events=['high_tap', 'toilet', 'shower'],
            show_time = True,
            dimension = sp
        )
        self.models = md
        #If we do not specify a slice, then we train on df1; if we specify df or a slice, then we train on the specified slice.
        if df is None:
            md.learning(self.df1)
        else:
            md.learning(df)
        md.get_union()
   
#===================================================================================================
#===================================================================================================

        # rowCount = int(math.sqrt(len(md.encoded))) + 2
        # colCount = int(math.sqrt(len(md.encoded)))
        # mas = [[0]*colCount for i in range(rowCount)]
        # print(len(md.encoded))
        
        # In order to decorate the array, we call the array recalculation, and then pass it to the loop for decorating
        mas = self.calculate_array(md.encoded)
        # These variables are needed for encoder decorating boundaries.
        first = len(md.buff[0])
        second = len(md.buff[1])
        summ = first + second
        print first,second,summ

        # The cycle is responsible for the fact that the visualization of the encoder would be square
        # for iterator in range(len(md.encoded)):  
        #     row = int(iterator / colCount)
        #     col = int(iterator % colCount)
        #     mas[row][col] = md.encoded[iterator] 


        # The cycle is responsible for decoder decoder. first is the edge of the first encoder in enc, summ is the edge of the first and second encoder
        count = 0
        for it in range(len(mas)):
            for i in range(len(mas[it])):
                count = count + 1 
                if count < first:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.35
                    if mas[it][i] == 1:
                         mas[it][i] = 0.1
                if count >= first and count < summ:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.9
                    if mas[it][i] == 1:
                        mas[it][i] = 0.7   
                if count >= summ:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.5
                    if mas[it][i] == 1:
                        mas[it][i] = 1
                 
        self.enc_array = mas
#===========================================================================================================    
#===========================================================================================================    
        # Part of the code is similar to the encoder and which is responsible for the visualization of sp (so that b sp was square)
        # rowSpCount = int(math.sqrt(len(md.puller))) + 2
        # colSpCount = int(math.sqrt(len(md.puller)))
        # pullerMas = [[0]*colSpCount for i in range(rowSpCount)]

        # for iterator in range(len(md.puller)):  
        #     row = int(iterator / colSpCount)
        #     col = int(iterator % colSpCount)
        #     pullerMas[row][col] = md.puller[iterator]  
    
        # self.sp_array = pullerMas

        # Recalculation for the SP re-paint (left here for not to rewrite the code in Jupyter)
        self.sp_array = self.calculate_array(md.puller)
        

    def remove_files(self):
        # The method that implements the removal of files from the folder
		os.remove('/var/union-classifier/data/high_tap-union.npy')
		os.remove('/var/union-classifier/data/sp.tmp')
		os.remove('/var/union-classifier/data/shower-union.npy')
		os.remove('/var/union-classifier/data/toilet-union.npy')
		print('REMOVE')

    def classify_model(self,df = None, verbosity=False):
        # Method for classifying a model
        if df is None:
            self.models.classify(self.df1)
        else:
            self.models.classify(df)

    def get_union(self,row_union= None):
        # Method to get the desired Union string
        union_s = self.models.union[row_union].getUnion()
        return union_s

   


   


    
