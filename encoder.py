import numpy
import nupic
from nupic.encoders.scalar import ScalarEncoder


class Encoder:
	def __init__(self, w=21, resolution=0.005, name=None):
		self.encoder = ScalarEncoder(
			w=w,
			minval=0,
			maxval=1,
			periodic=False,
			resolution=resolution,
			name=name,
			forced=False,
			clipInput=True)
		
		self.width = self.encoder.getWidth()
		self.encoded = numpy.zeros(self.width)
	
	def get_width(self):
		return self.width
	
	def encode_to_array(self, val):
		self.encoder.encodeIntoArray(val, self.encoded)
		return self.encoded
		
	def decode(self, val):
		name = self.encoder.getScalarNames()[0]
		decoded = self.encoder.decode(val)
		
		# print decoded[0][name][1]
		# print '***'
		
		return decoded[0][name][1]


