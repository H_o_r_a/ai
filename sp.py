import os
import numpy

from nupic.algorithms.spatial_pooler import SpatialPooler


class SP:
	def __init__(self, width=2048, dimension=2048):
		self.width = width
		self.dimension = dimension
		if self.dimension > self.width:
			self.dimension = self.width
		
		curr_dir = os.path.dirname(os.path.abspath(__file__))
		self.sp_file_path = curr_dir + '/data/sp.tmp'
		
		self.sp = self.create()
		
	def create(self):
		if os.path.isfile(self.sp_file_path):
			return self.loadModel()
		else:
			#print self.width
			return SpatialPooler(
				# How large the input encoding will be.
				inputDimensions=(self.width,),
				# How many mini-columns will be in the Spatial Pooler.
				columnDimensions=(self.dimension,),
				# What percent of the columns's receptive field is available for potential
				# synapses?
				potentialPct=0.85,
				# This means that the input space has no topology.
				globalInhibition=1.0,
				localAreaDensity=-1.0,
				# Roughly 2%, giving that there is only one inhibition area because we have
				# turned on globalInhibition (40 / 2048 = 0.0195)
				numActiveColumnsPerInhArea=80.0,
				# How quickly synapses grow and degrade.
				synPermInactiveDec=0.005,
				synPermActiveInc=0.04,
				synPermConnected=0.1,
				# boostStrength controls the strength of boosting. Boosting encourages
				# efficient usage of SP columns.
				boostStrength=1.0,
				# Random number generator seed.
				seed=1995,
				# Determines if inputs at the beginning and end of an input dimension should
				# be considered neighbors when mapping columns to inputs.
				wrapAround=False
			)
	
	def compute(self, encoding, learning=True):
		# Create an array to represent active columns, all initially zero. This
		# will be populated by the compute method below. It must have the same
		# dimensions as the Spatial Pooler.
		active_columns = numpy.zeros(self.dimension)
		
		# Execute Spatial Pooling algorithm over input space.
		self.sp.compute(encoding, learning, active_columns)
		
		return active_columns
	
	def save(self):
		with open(self.sp_file_path, "wb") as sp_file:
			self.sp.writeToFile(sp_file)
	
	def loadModel(self):
		with open(self.sp_file_path) as sp_file:
			return SpatialPooler.readFromFile(sp_file)


