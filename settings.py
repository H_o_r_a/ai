import os
import math
import time
import pandas
import numpy
import __future__
import matplotlib 
import numpy as np
from model import Model
import ipywidgets as widgets
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from IPython.display import display
from ipywidgets import interact,interactive
from preprocessing_data import Data_Preparation as DP

class Settings:
    
    def __init__(self):
        self.curr_dir = os.path.dirname('/var/union-classifier/test.py')
        print (self.curr_dir)
        self.sp_file_path = self.curr_dir + '/data/sp.tmp'
        self.df1 = None
        self.df2 = None
        self.df3 = None
        self.enc_array = []
        self.sp_array = []
        self.first_item = 0
        self.second_item = 0
        self.readData()
    
    # def learning(self,model, df=None, idx=1):
    #     model.learning(df, idx)

    #     u_tp = numpy.load(self.curr_dir + '/data/high_tap-union.npy')
    #     u_sh = numpy.load(self.curr_dir + '/data/shower-union.npy')
    #     u_tl = numpy.load(self.curr_dir + '/data/toilet-union.npy')

    #     print ('******')
    #     print ('Crosses')
    #     print ('******')
    #     print ('shower - tap')
    #     print (md.union['shower'].check(u_tp))
    #     print ('shower - toiler')
    #     print (md.union['shower'].check(u_tl))
    #     print ('toilet - tap')
    #     print (md.union['high_tap'].check(u_tl))

    #     print ('******')
    #     print ('Sizes')
    #     print ('******')
    #     # print numpy.nonzero(u_tp)
    #     # print numpy.nonzero(u_sh)
    #     # print numpy.nonzero(u_tl)

    #     print ('tap   ', numpy.nonzero(u_tp)[0].size)
    #     print ('shower', numpy.nonzero(u_sh)[0].size)
    #     print ('toilet', numpy.nonzero(u_tl)[0].size)
   
    def readData(self):
        data_file_path1 = self.curr_dir + '/222/shower.csv'
        data_file_path2 = self.curr_dir + '/222/tap.csv'
        data_file_path3 = self.curr_dir + '/222/toilet.csv'

        data1 = pandas.read_csv(data_file_path1)
        prepare_data1 = DP(data1)

        data2 = pandas.read_csv(data_file_path2)
        prepare_data2 = DP(data2)

        data3 = pandas.read_csv(data_file_path3)
        prepare_data3 = DP(data3)

        self.df1 = prepare_data1.prepare()
        print ('*'*100)
        self.df2 = prepare_data2.prepare()
        print ('*'*100)
        self.df3 = prepare_data3.prepare()
        print ('*'*100)
    

    def redrawing(self,array, string):
        fig = plt.figure(figsize = (10, 10))
        ax = sns.heatmap(array, linewidths = 1 ,linecolor = "w",cbar = False,cmap = "nipy_spectral")
        ax.set_title(string)
        plt.gca().invert_yaxis()
        plt.axis("off")

    def createModel(self,enc = 1,sp = 2048):
        print('ENC:'), enc
        print('SP_COL'),sp
        md = Model(
            encoders=[
                {'name': "flow", 'resolution': enc},
                {'name': "pressure", 'resolution': enc},
                {'name': "duration", 'resolution': enc}
            ],
            events=['high_tap', 'toilet', 'shower'],
            show_time = True,
            dimension = sp
        )
        md.learning(self.df1)
   
#===================================================================================================
#===================================================================================================

        rowCount = int(math.sqrt(len(md.encoded))) + 2
        colCount = int(math.sqrt(len(md.encoded)))
        mas = [[0]*colCount for i in range(rowCount)]
        print(len(md.encoded))
        first = len(md.buff[0])
        second = len(md.buff[1])
        summ = first + second
        print first,second,summ

        for iterator in range(len(md.encoded)):  
            row = int(iterator / colCount)
            col = int(iterator % colCount)
            mas[row][col] = md.encoded[iterator] 
    
        count = 0
        for it in range(len(mas)):
            for i in range(len(mas[it])):
                count = count + 1 
                if count < first:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.35
                    if mas[it][i] == 1:
                         mas[it][i] = 0.1
                if count >= first and count < summ:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.9
                    if mas[it][i] == 1:
                        mas[it][i] = 0.7   
                if count >= summ:
                    if mas[it][i] == 0:
                        mas[it][i] = 0.5
                    if mas[it][i] == 1:
                        mas[it][i] = 1
                 
        # fig = plt.figure(figsize = (10, 10))
        # ax = sns.heatmap(mas, linewidths = 1 ,linecolor = "w",cbar = False,cmap = "nipy_spectral")
        # plt.gca().invert_yaxis()
        # plt.axis("off")
        #self.redrawing(mas,'ENCODER')
        self.enc_array = mas
#===========================================================================================================    
#===========================================================================================================    
        rowSpCount = int(math.sqrt(len(md.puller))) + 2
        colSpCount = int(math.sqrt(len(md.puller)))
        pullerMas = [[0]*colSpCount for i in range(rowSpCount)]

        for iterator in range(len(md.puller)):  
            row = int(iterator / colSpCount)
            col = int(iterator % colSpCount)
            pullerMas[row][col] = md.puller[iterator]  
    
        self.sp_array = pullerMas
        # fig = plt.figure(figsize = (10, 10))
        # ax = sns.heatmap(pullerMas, linewidths = 1 ,linecolor = "w",cbar = False,cmap = "nipy_spectral")
        # plt.gca().invert_yaxis()
        # plt.axis("off")
        # print('Len Sp: '),len(md.puller)
        #self.redrawing(pullerMas,'SP')


    
